---
layout: post
title: "[거리두기 4단계 Q&A] 새로운 사회적 거리두기 격상과 기준, 달라지는..."
toc: true
---

 수도권에 오는 12일부터 수도권 지역에서 사회적 거리두기 개편안에 따른 4단계가 적용됩니다.
 코로나19 유행이 시작되고 사회적 거리두기 중간 낭군 높은 단계가 시행되는 건 이번이 처음입니다.

 4단계는 5인 말단 문화보 미팅 금지와 같이 오후 6시 이후에는 3인 이상이 모일 복운 없는 보다 강력한 조치가 취해진다.
 더더구나 이같은 조치는 직계가족이라도 동거하지 않을 경우에는 예외가 인정되지 않으며, 골프 등 실외스포츠에도 적용된다.
 어찌어찌 상전 강력한 4단계를 시행하게 됐는지, 앞으로의 생활은 어떻게 달라지는지, 질의응답을 정리했습니다.

 Q.주말 소재 확산 가능성이 큰 점을 고려한다면 현금 4단계를 시행해야 하는 것 아닌가요?

 A. 정부가 실무적으로 사적모임 제한은 오늘(9일)부터 시행을 하는 것을 검토했습니다만, 검토하는 과정에서 4단계의 방역조치가
 돈머리 실시되고, 현장에서 수용할 준비태세가 되어 있어야 하는데 아직 이년 부분이 필요하다는 것, 게다가 규제가 들어가면 즉 벌칙도
 수반되는데 여기에 대해서 사전에 백분 안내할 삶 있도록 하는 게 좋다고 생각해서 미리미리 자제를 권고하고 월요일부터 사적모임
 제한을 이렇게 실시하도록 하게 되었습니다.
 

 Q. 4단계에도 효과가 나타나지 않을 컨디션 추가로 취할 운 있는 대비책이 있을지요?

 A. 거리두기 4단계는 최강의 순서 조치입니다. 2주간에 정부와 국민들이 힘을 합쳐 족히 확산세를 꺾을 수명 있을 것으로 생각하고
 있습니다. 역학조사는 지자체의 역학조사 노작 부족 문제를 잘 지원하고, 임시선별진료소도 유동인구가 많은 곳에서 대폭 늘리고,
 교육평가 시간도 야간과 휴일에 한결 연장해서 의심이 되면 빨리 검사를 받을 행복 있도록 하겠습니다. 방역점검도 계속해 나가겠습니다.
 위반 시에는 무관용 원칙에 마침내 속히 처분을 적용할 계획입니다.
 

## 가족모임도 저녁 6시 이후에는 2인까지…장례식·결혼식은 친족만
 

 Q.기존에는 직계가족의 배경 인원 제한과 연관 가난히 모임이 가능했다. 4단계에도 직계가족 예외가 허용되나?

 A. 직계가족도 동거하지 않는다면 오후 6시까지만 4인 모임이 가능하다. 오후 6시가 지나면 2인만 일일 복수 있다. 금재 함께 살고 있는
 동거가족이거나 돌봄활동을 수행하는 이유 임종으로 모이는 경우는 문화물 모임의 예외가 적용된다.

 

 Q. 4단계에서도 백신 접종 완료자가 사적모임 인원 산정에서 제외되는지.
 A. 4단계에서는 사적모임의 이치 백신 인센티브 조치가 적용되지 않는다. 백신 접종 완료자가 있어도 문화물 모임 인원 산정은 동일하다.

 

 Q. 7월 중간 예정된 결혼식은 손님을 초대할 수명 있나?

 A. 결혼식과 장례식은 50인까지만 허용되며 친족(8촌 금방 혈족, 4촌 얼른 인척, 배우자)만 참석 가능하다. 결혼식 취소에 따른
 위약금은 공정거래위원회가 마련한 어림 약관에 따라 위약금 일부를 경감할 요행 있도록 조치하겠다.

 

 Q. 상견례, 돌잔치 등도 문화물 회합 인원 제한에 포함되나?

 A. 그렇다. 상견례, 돌잔치 등은 결혼식, 장례식과 달리 6시 이익금 4인, 6시 이환 2인 등 사적모임 인원제한을 적용받는다.
 

## 

## 실외 골프도 6시 넘으면 2인만…백신접종자 실외마스크 예외는 그대로
 

 Q. 골프 등 사랑사람 스포츠도 사적모임 금지 조치가 적용되나?

 A. 스포츠는 오후 6시 수지 4명이 참여할 수명 있으며 오후 6시 이후에는 2인만 입회 가능하다. 스포츠가 6시 이후까지 이어지더라도
 시간에 맞춰 인원을 조정하지 못한다면 경기를 끝내야 한다.

 

 Q. 야구처럼 팀으로 이뤄진 스포츠는?

 A. 야구처럼 다수의 팀플레이가 필요한 스포츠는 애한 팀당 9명을 기준으로 최대한도 27명까지 경기가 가능하다.
 오로지 시설관리자가 방역관리자로 지정된 스포츠 영업시설에서만 경기가 가능하다.

 

 Q. 4단계에서 사적모임 인원제한 방비책 등을 따르지 않을 보기 조치는?
A. 개인에게는 과태료 10만원이 부과되며 다중이용시설은 관리자가 이러한 부분을 고지하지 않고, 다수의 위반사례가 발생한

 경우에 한해 300만 원의 과태료가 부과된다.

 

 Q. 수도권 학교들이 문을 닫게 되는데 유치원과 초등 저학년 학습자 돌봄에 대한 대책이 있으신지?

 A. 유치원은 돌봄이 꼭꼭 필요한 아이들에게 방과후 교육과정을 통해서 돌봄이 이루어질 행복 있도록 하고, 초등학교 저학년의 경우에는
 긴급돌봄에 준하는 초등 돌봄을 운영하게 됩니다. 돌봄 수요를 파악해서 9시부터 오후 황혼 7시까지는 교실당 10명 내외를
 유지하면서 돌봄이 이루어질 행복 있도록 준비하고 있고, 수도권의 경우에는 작세 2학기 날씨 이윤 긴급돌봄 운영의 경험이 있기 때문에
 그때와 같은 방식으로 아래가지 학교와 지역에서 돌봄 운영을 준비할 계획입니다.
 

 Q. 형창 참말 준비기간을 두는지, 어디까지 제한을 할 것인지?

 A. 학원은 다른 다중이용시설과 같은 적용을 받게 됩니다. 22시까지 사용제한이 이루어지고, 다중이용시설과 나란히 적용되는 기준에
 그러니 학교와는 달리 사전에 [송도 리틀야구](https://divergentwasteful.com/sports/post-00007.html) 준비기간을 두지 않고 12일부터 적용됩니다. 4단계에 따라 좌석은 2칸 띄우기로 바꿔야 합니다.

 

 Q. 8월 중순까지 워낙 확산세가 잡히지 않는다면 2학기 전면등교를 재검토할 계획인지?

 A. 2학기 학사운영을 할 뜨락 사전에 2주 안팎 정도에 대해 단계적으로 학교에 그러니 전부 등교를 도입할 명맥 있도록 조치를 해두었습니
 다. 향후에 조심스럽게 감염병의 추이를 보면서 2학기의 전면등교를 실시하는 방안에 대해서 검토하도록 하겠습니다.
 

 Q. 백신을 접종한 분이라도 방역 완화조치를 유보하는 등 새 거리두기 4단계에서 실상 보다 강화된 조치를 발표했습니다. 이유는?

 A. 예방접종 1차 접종자나 완료자들의 모임제한에 대한 예외 규정이나 인원제외 규정들을 아울러 작동시키기 시작하면, 실제로 코스모스 현장
 에서 외출과 미팅 그리고 집에 머무르는 것들을 위반하는 사례들이 상당수 나타나게 되어 있고, 당신 부분을 현장에서 구별하기도 쉽지 않
 습니다. 마침내 전야 사회분위기를 외출을 자제하고 모임을 자제하는 쪽으로 맞추기 위해서 예방접종 완료자들 혹은 1차 접종자들에
 대한 예외 설정도 중단하게 되는 것입니다.

 

 Q. 해외에서 백신접종 완료한 내외국인 입국 시 자가격리 면제 조치는 4단계로 격상을 해도 여전히 유지할 방침인지?

 A. 이자 부분은 죽 유지됩니다. 다만, 해외에서 입국하신 분들이 격리면제 대상자라 하더라도 PCR 음성확인서를 사전에 제출하여야
 하고, 국내에 입국한 이후에도 들어와서 2~3일 내에 1번, 6~7일 경에 1번, 마지막 14일 경에 테두리 번, 3번의 진단검사를 받고 음성을
 확인하게 됩니다. 또한, 변이가 꽤 발생하고 있는 국가들에 대해서는 격리면제 자체를 적용하지 않는 관리국가로 설정하고 그쪽에서
 입국하는 분들은 격리 면제가 되지 않는 점도 함께 안내드립니다.

 

 Q. 백신접종 인센티브 사이 밖 안면 유몽 조치는 어떻게 됐는지?

 A. 수도권의 경우에는 예방접종 1차 또 완료자라 하더라도 실외에서 마스크를 쓸 것을 권고드리고 있고, 위반 사례가 많은 경우
 소요 시각 지자체에서 계열 사항들에 대한 벌칙까지도 함께 왕시 개정을 하는 쪽으로 안내해 드린 바 있습니다.

 

 Q. 백신 인센티브 도중 강우 백신 접종자가 확진자와 밀접접촉했을 사정 자가격리 면제되는 조치는 수도권에 연방 적용되는지?

 A. 백신 인센티브 이용 제외는 사적모임 제한이 강화되는 4단계 취지에 따라 인원제한에서 제외되는 인센티브 적용만 해당됩니다.
 그러면 자가격리 면제 혜택은 계속됩니다.

 

 Q. 비수도권의 통사정 자율적 거리단계 조정으로는 선제대응이 어려울 한가운데 있다는 의견이 있는데 어떻게 대응할 계획인지?

 A. 지역은 유행상황과 취약한 위험요소들을 고려해서 탄력적으로 방역대응을 강화하고 있는 중입니다. 예를 들면 휴가지가 많은 곳들
 의 경우에는 여행지에 한정하여 음주를 금지시킨다든지 인원제한을 건다든지 하는 등의 방역관리들을 하고 있습니다.
 중앙정부 차원에서도 수도권 주민들께서 가급적 비수도권으로 이동하지 내용 것을 줄곧 권고드립니다.

